package khartn.screenshotuploader.Desctop.Tray;

import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.URL;
import javax.swing.JOptionPane;
import khartn.screenshotuploader.MainFrame;
import khartn.screenshotuploader.ScreenshotHandler;

public class TrayMenuHandler {

    public void add(final MainFrame mainFrame, final ScreenshotHandler screenshotHandler) {
        SystemTray tray;
        String imgName;
        URL imgURL;
        Image image;
        PopupMenu popup;
        ActionListener exitListener;
        MenuItem defaultItem;
        ActionListener makeSCListener;
        ActionListener showInfoMessage;
        ActionListener showMainForm;
        MenuItem makeSCItem;
        MenuItem showInfoMessageItem;
        MenuItem showMainFormItem;
        final TrayIcon trayIcon;

        try {
            tray = SystemTray.getSystemTray();

            imgName = "favicon_1.jpg";
            imgURL = getClass().getResource(imgName);

            image = Toolkit.getDefaultToolkit().getImage(imgURL);

            popup = new PopupMenu();

            exitListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    System.out.println(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("EXITING..."));
                    System.exit(0);
                }
            };

            defaultItem = new MenuItem(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ВЫХОД"));
            defaultItem.addActionListener(exitListener);
            popup.add(defaultItem);

            showInfoMessage = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    JOptionPane.showMessageDialog(null, "ПОСЛЕ СВОРАЧИВАНИЯ ОКНА ПРОГРАММА БУДЕТ ДОСТУПНА В ТРЕЕ. ЕСЛИ ПРОГРАММА БЫЛА СВЁРНУТА В ТРЕЙ, ТО СКРИНШОТ МОЖНО СДЕЛАТЬ НАЖАТИЕМ Print Screen.");
                }
            };

            showInfoMessageItem = new MenuItem(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ПОДСКАЗКА"));
            showInfoMessageItem.addActionListener(showInfoMessage);
            popup.add(showInfoMessageItem);

            showMainForm = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    MainFrame.mainFrame.setVisible(true);
                }
            };

            showMainFormItem = new MenuItem("Отобразить главную форму");
            showMainFormItem.addActionListener(showMainForm);
            popup.add(showMainFormItem);

            makeSCListener = new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    screenshotHandler.make(mainFrame);
                }
            };

            makeSCItem = new MenuItem(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("СДЕЛАТЬ СКРИНШОТ"));
            makeSCItem.addActionListener(makeSCListener);
            popup.add(makeSCItem);

            trayIcon = new TrayIcon(image, java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("TRAY DEMO"), popup);

            trayIcon.setImageAutoSize(true);

            tray.add(trayIcon);
        } catch (Exception e) {
            MainFrame.mainLogger.fatal(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("TRAYICON COULD NOT BE ADDED. "), e);
        }
    }
}
