package khartn.screenshotuploader;

import khartn.screenshotuploader.StartExternalPrograms.CmdProcessBuilder;

/**
 *
 * @author Arthur
 */
public class ScreenshotHandler {

    public void make(MainFrame mainFrame) {
        try {
            ScreenshotHandlerThread screenshotHandlerThread = new ScreenshotHandlerThread(mainFrame);
            screenshotHandlerThread.start();
        } catch (Exception ex) {
            MainFrame.mainLogger.fatal("ERROR IN SCREENSHOTHANDLER: ",ex);
        }
    }
    public void make(MainFrame mainFrame, Boolean showFormAfter) {
        try {
            ScreenshotHandlerThread screenshotHandlerThread = new ScreenshotHandlerThread(mainFrame, showFormAfter);
            screenshotHandlerThread.start();
        } catch (Exception ex) {
            MainFrame.mainLogger.fatal(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ERROR IN SCREENSHOTHANDLER: {0}"), new Object[] {ex}));
        }
    }

    public void edit() {
        try {
            CmdProcessBuilder.main();
        } catch (Exception ex) {
            MainFrame.mainLogger.fatal(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ERROR IN SCREENSHOTHANDLER: "), ex);
        }
    }
}


