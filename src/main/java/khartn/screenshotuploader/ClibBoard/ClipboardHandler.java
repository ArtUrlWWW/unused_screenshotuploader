package khartn.screenshotuploader.ClibBoard;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import khartn.screenshotuploader.MainFrame;

/**
 *
 * @author Arthur
 */
public class ClipboardHandler {
     public void setClipboardContents(String aString) {
        try {
            StringSelection stringSelection = new StringSelection(aString);
            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            clipboard.setContents(stringSelection, null);
        } catch (Exception ex) {
            MainFrame.mainLogger.fatal(java.text.MessageFormat.format(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ERROR IN SCREENSHOTHANDLER: {0}"), new Object[] {ex}));
        }
    }
    
}
