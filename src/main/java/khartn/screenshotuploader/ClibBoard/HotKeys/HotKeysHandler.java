package khartn.screenshotuploader.ClibBoard.HotKeys;

import khartn.screenshotuploader.Desctop.Tray.TrayMenuHandler;
import khartn.screenshotuploader.MainFrame;
import khartn.screenshotuploader.ScreenshotHandler;
import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class HotKeysHandler implements NativeKeyListener {

    MainFrame mainFrame;
    ScreenshotHandler screenshotHandler;

    public HotKeysHandler(final MainFrame mainFrame, final ScreenshotHandler screenshotHandler) {
        this.mainFrame = mainFrame;
        this.screenshotHandler = screenshotHandler;
    }

    public void add() {
        try {
            TrayMenuHandler trayMenuHandler = new TrayMenuHandler();
            trayMenuHandler.add(mainFrame, screenshotHandler);
        } catch (Exception ex) {
            MainFrame.mainLogger.fatal(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ERROR IN HOTKEYSHANDLER: "), ex);
        }

        try {
            GlobalScreen.registerNativeHook();
        } catch (NativeHookException ex) {
            MainFrame.mainLogger.fatal("There was a problem registering the native hook.", ex);
        }                  //Construct the example object and initialze native hook.   
        GlobalScreen.getInstance().addNativeKeyListener(new HotKeysHandler(mainFrame, screenshotHandler));
    }

    public void nativeKeyPressed(NativeKeyEvent e) {
    }

    public void nativeKeyReleased(NativeKeyEvent e) {
        try {
//            MainFrame.mainLogger.info("Key Released: !!!" + NativeKeyEvent.getKeyText(e.getKeyCode()) + "!!!");
            if (NativeKeyEvent.getKeyText(e.getKeyCode()).equals("Print Screen")) {
//                MainFrame.mainLogger.info("++++++++++++++++++++++++++++++");
                screenshotHandler.make(mainFrame);
            }
        } catch (Exception ex) {
            MainFrame.mainLogger.fatal("Some error: ", ex);
        }
    }

    public void nativeKeyTyped(NativeKeyEvent e) {
    }
}
