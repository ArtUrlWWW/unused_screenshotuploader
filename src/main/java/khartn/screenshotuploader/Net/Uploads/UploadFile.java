package khartn.screenshotuploader.Net.Uploads;

import java.io.File;
import java.nio.charset.Charset;
import khartn.screenshotuploader.MainFrame;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.MultipartEntity;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;

public class UploadFile {

    public String upload(String URL, String pathToFile) throws Exception {
        String output = "";
        File file;
        HttpClient httpclient;
        HttpPost httppost;
        MultipartEntity mpEntity;
        ContentBody cbFile;

        try {

            httpclient = new DefaultHttpClient();
            httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);
            httpclient.getParams().setParameter(CoreProtocolPNames.HTTP_CONTENT_CHARSET, "UTF-8");

            httppost = new HttpPost(URL);
            file = new File(pathToFile);

            mpEntity = new MultipartEntity();
            cbFile = new FileBody(file, "image/png");
            mpEntity.addPart("uploadedFile", cbFile);
            mpEntity.addPart("collectionName", new StringBody(MainFrame.collectionName.getText(), Charset.forName("UTF-8")));
            mpEntity.addPart("MLfunct", new StringBody("createItemFromUpload"));

            httppost.setEntity(mpEntity);
            HttpResponse response = httpclient.execute(httppost);
            HttpEntity resEntity = response.getEntity();

            if (resEntity != null) {
                output = EntityUtils.toString(resEntity);
            }
            if (resEntity != null) {
                resEntity.consumeContent();
            }

            httpclient.getConnectionManager().shutdown();
        } catch (Exception ex) {
            MainFrame.mainLogger.fatal(java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ERROR IN UPLOADFILE: ") + ex);
        }
        return output;
    }
}
