package khartn.screenshotuploader.Net.SelfUpdate;

import khartn.screenshotuploader.ClibBoard.ClipboardHandler;
import java.io.File;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import khartn.screenshotuploader.MainFrame;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Hello world!
 *
 */
public class UpdateHandler {

    public void doit() {
        Integer remoteRevisionNumber;
        Integer localRevisionNumber;
        Attributes manifestEntries;
        Document doc;
        Elements newsHeadlines;
        String revisionStr;
        String revisionStrOrig;
        JarFile jarfile;

        try {
            String classPath = getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
            if ((new File(classPath)).isFile()) {
                jarfile = new JarFile(classPath);
                Manifest manifest = jarfile.getManifest();
                manifestEntries = manifest.getMainAttributes();
                localRevisionNumber = Integer.parseInt(manifestEntries.getValue("Version"));

                doc = Jsoup.connect("http://code.google.com/p/my-downloads/downloads/list").get();
                newsHeadlines = doc.select(".ifOpened .col_0 a");
                for (Element el : newsHeadlines) {
//                    System.out.println(el.html());
                    if (el.html().indexOf("SCU_rev.") > -1) {
                        revisionStrOrig = el.html();
                        revisionStr = revisionStrOrig.replace("SCU_rev.", "").replace(".zip", "");
                        remoteRevisionNumber = Integer.parseInt(revisionStr);
////                System.err.println(remoteRevisionNumber);

                        if (remoteRevisionNumber > localRevisionNumber) {

                            doc = Jsoup.connect("http://code.google.com/p/my-downloads/downloads/" + el.attr("href")).get();
                            revisionStrOrig = "http:" + doc.select(".box-inner a").get(1).attr("href");
//                            System.out.println(doc.select(".box-inner a").get(1).attr("href"));
                            UIManager.put("OptionPane.yesButtonText", "Да");
                            UIManager.put("OptionPane.noButtonText", "Нет");
                            if (JOptionPane.showConfirmDialog(null, "Доступна новая версия программы. Рекомендуется скачать новую версию программы. Напоминание: новую версию программы лучше распаковывать в отдельную от старой версии папку. Скачать?", "KhArtNs ScreenshotUploader", JOptionPane.YES_NO_OPTION) == 0) {
                                ClipboardHandler clipboardHandler = new ClipboardHandler();
                                clipboardHandler.setClipboardContents(revisionStrOrig);
                                if (JOptionPane.showConfirmDialog(null, "Ссылка на файл скопирована в буфер обмена. Открыть её в браузере?", "KhArtNs ScreenshotUploader", JOptionPane.YES_NO_OPTION) == 0) {
                                    java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
                                    java.net.URI uri = new java.net.URI(revisionStrOrig);
                                    desktop.browse(uri);
                                }
                            }
                        }

                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            MainFrame.mainLogger.fatal("Error in UpdateHandler: ", ex);
        }
    }
}
