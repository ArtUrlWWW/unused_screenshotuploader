package khartn.screenshotuploader.StartExternalPrograms;

import java.io.*;
import java.util.*;
import khartn.screenshotuploader.MainFrame;

public class CmdProcessBuilder {

    public static void main() {
        try {
            List<String> command = new ArrayList<String>();
            command.add("jre\\bin\\java");
            command.add("-jar");
            command.add("lib\\ImageEditor.jar");
            command.add("screen.jpg");

            ProcessBuilder builder = new ProcessBuilder(command);
            //        Map<String, String> environ = builder.environment();
            //        builder.directory(new File(System.getenv("temp")));
            //        System.out.println((new File("")).getAbsolutePath());
            builder.directory();

            //        System.out.println("Directory : " + System.getenv("temp"));
            final Process process = builder.start();
            InputStream is = process.getInputStream();
            InputStreamReader isr = new InputStreamReader(is);
            BufferedReader br = new BufferedReader(isr);
            String line;
            while ((line = br.readLine()) != null) {
                //            System.out.println(line);
            }
            //        System.out.println("Program terminated!");
        } catch (IOException ex) {
            MainFrame.mainLogger.fatal("Error: ", ex);
        }
    }
}
