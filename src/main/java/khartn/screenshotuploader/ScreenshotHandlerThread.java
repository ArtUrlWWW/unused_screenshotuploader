package khartn.screenshotuploader;

import khartn.screenshotuploader.Net.Uploads.UploadFile;
import khartn.screenshotuploader.ClibBoard.ClipboardHandler;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.File;
import java.util.Date;
import javax.imageio.ImageIO;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Arthur
 */
public class ScreenshotHandlerThread extends Thread {

    MainFrame mainFrame;
    Boolean showFormAfter = false;

    public ScreenshotHandlerThread(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
    }

    public ScreenshotHandlerThread(MainFrame mainFrame, Boolean showFormAfter) {
        this.mainFrame = mainFrame;
        this.showFormAfter = showFormAfter;
    }

    @Override
    public void run() {
        try {
            this.sleep(1000);

            Rectangle screenRect = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
            BufferedImage capture = new Robot().createScreenCapture(screenRect);
            ImageIO.write(capture, "jpg", new File("screen.jpg"));
            ScreenshotHandler screenshotHandler = new ScreenshotHandler();
            screenshotHandler.edit();

            if (MainFrame.askQuestionAboutUploadingToTheSite.isSelected()) {
                JCheckBox checkbox = new JCheckBox("Не показывать данное сообщение в дальнейшем.");
                Object[] params = {"Выгрузить на arturik.net?", checkbox};

                if (JOptionPane.showConfirmDialog(null, params, "KhArtNs ScreenshotUploader", JOptionPane.YES_NO_OPTION) == 0) {
                    MainFrame.uploadToTheSite = true;
                } else {
                    MainFrame.uploadToTheSite = false;
                }

                if (checkbox.isSelected()) {
                    MainFrame.askQuestionAboutUploadingToTheSite.setSelected(false);
                }
            }

            if (MainFrame.uploadToTheSite) {
                UploadFile uploadFile = new UploadFile();
                String uploadResult = uploadFile.upload("http://arturik.net/bitrix/templates/khartn/third_party_libs/ckeditor/plugins/grabImages/mediaLibHandler.php?t=" + (new Date().getTime()), "screen.jpg");
                if (uploadResult.equals("") || uploadResult.contains("error")) {
                    MainFrame.mainLogger.info(uploadResult);
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("ВОЗНИКЛА ОШИБКА ПРИ СОЗДАНИИ И ЗАГРУЗКЕ СКРИНШОТА. ПОЖАЛУЙСТА, ПОПРОБУЙТЕ ЕЩЁ РАЗ."));
                } else {
                    ClipboardHandler clipboardHandler = new ClipboardHandler();
                    clipboardHandler.setClipboardContents(uploadResult);
                    JOptionPane.showMessageDialog(null, java.util.ResourceBundle.getBundle("khartn/screenshotuploader/Bundle").getString("СКРИНШОТ УСПЕШНО ЗАГРУЖЕН И ССЫЛКА СКОПИРОВАНА В БУФЕР ОБМЕНА."));
                }
            } else {

                if (MainFrame.askAboutFilePathCopy.isSelected()) {
                    JCheckBox checkbox = new JCheckBox("Не показывать данное сообщение в дальнейшем.");
                    Object[] params = {"Файл сохранён в папке: " + (new File("")).getAbsolutePath() + " . Сохранить путь до папки в буфере обмена?", checkbox};

                    if (JOptionPane.showConfirmDialog(null, params, "KhArtNs ScreenshotUploader", JOptionPane.YES_NO_OPTION) == 0) {
                        MainFrame.copyFilePathToClipboard = true;
                    } else {
                        MainFrame.copyFilePathToClipboard = false;
                    }

                    if (checkbox.isSelected()) {
                        MainFrame.askAboutFilePathCopy.setSelected(false);
                    }
                }

                if (MainFrame.copyFilePathToClipboard) {
                    ClipboardHandler clipboardHandler = new ClipboardHandler();
                    clipboardHandler.setClipboardContents((new File("")).getAbsolutePath());
                }
            }

            if (this.showFormAfter) {
                mainFrame.setVisible(true);
            }

        } catch (Exception ex) {
            MainFrame.mainLogger.fatal("Some error:", ex);
        }
    }
}
